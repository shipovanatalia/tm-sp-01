package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.Status;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @Nullable
    private String projectId;

    @Nullable
    private String name;

    @Nullable
    private String description;

    //костыльный конструктор
    public Task(String id, String name, String projectId, String description, Status status) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.description = description;
        this.status = status;
    }
}

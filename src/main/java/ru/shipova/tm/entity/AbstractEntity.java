package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEntity {

    @NotNull
    String id = "";

    @Nullable
    Status status;

    @Nullable
    Date dateOfBegin;

    @Nullable
    Date dateOfEnd;

    @Nullable
    Date dateOfCreate;
}

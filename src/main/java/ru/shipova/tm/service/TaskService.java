package ru.shipova.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;

@Service
@NoArgsConstructor
public final class TaskService {
    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    public List<Task> getListTask() {
        return taskRepository.findAll();
    }

    @Nullable
    public Task findOne(@Nullable final String id){
        return taskRepository.findOne(id);
    }

    public void create(final @Nullable Task task) {
        task.setId(UUID.randomUUID().toString());
        taskRepository.persist(task);
    }

    public void remove(@Nullable final Task task) {
        taskRepository.remove(task);
    }

    public void edit(@Nullable final Task task){
        taskRepository.merge(task);
    }
}

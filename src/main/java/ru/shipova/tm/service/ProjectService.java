package ru.shipova.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;

@Service
@NoArgsConstructor
public final class ProjectService{
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskRepository taskRepository;

    public List<Project> getListProject() {
        return projectRepository.findAll();
    }

    public void create(@Nullable final Project project) {
        project.setId(UUID.randomUUID().toString());
        projectRepository.persist(project);
    }

    public void remove(@Nullable final Project project) {
        projectRepository.remove(project);
        taskRepository.removeAllTasksOfProject(project);
    }

    @Nullable
    public Project findOne(@Nullable final String id){
        return projectRepository.findOne(id);
    }

    public void edit(@Nullable final Project project){
        projectRepository.merge(project);
    }
}

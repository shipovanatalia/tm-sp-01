package ru.shipova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Task;

import java.util.*;

@Repository
public final class TaskRepository extends AbstractRepository<Task> {

    public TaskRepository() {
        //Тестовые таски
        final String task1Id = UUID.randomUUID().toString();
        final String task1projectId = "111";
        final String task1name = "task1";
        final String task1description = "test task1";
        final Status task1status = Status.PLANNED;
        getMap().put(task1Id, new Task(task1Id, task1name, task1projectId, task1description, task1status));

        final String task2Id = UUID.randomUUID().toString();
        final String task2projectId = "222";
        final String task2name = "task2";
        final String task2description = "test task2";
        final Status task2status = Status.PLANNED;
        getMap().put(task2Id, new Task(task2Id, task2name, task2projectId, task2description, task2status));
    }

    @NotNull
    public List<String> showAllTasksOfProject(final String projectId) {
        final List<String> listOfTasks = new ArrayList<>();

        for (Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (projectId.equals(entry.getValue().getProjectId())) {
                listOfTasks.add(entry.getValue().getName());
            }
        }
        return listOfTasks;
    }


    @Override
    public void persist(final Task task) {
        if (isExist(task)) return;
        getMap().put(task.getId(), task);
    }

    public void removeAllTasksOfProject(final Project project) {
        final Iterator<Map.Entry<String, Task>> iterator = getMap().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (project.getId().equals(entry.getValue().getProjectId())) {
                iterator.remove();
            }
        }
    }
}

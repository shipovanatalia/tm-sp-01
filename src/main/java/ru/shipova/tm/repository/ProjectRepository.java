package ru.shipova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.entity.Project;

import java.util.Date;
import java.util.Map;

@Repository
public final class ProjectRepository extends AbstractRepository<Project> {

    public ProjectRepository() {
        //тестовые проекты
        final String project1Id = "111";
        final String project1name = "project1";
        final String project1description = "test project1";
        final Status project1status = Status.PLANNED;
        Project project = new Project(project1Id, project1name, project1description, project1status);
        project.setDateOfBegin(new Date());
        getMap().put(project1Id, project);

        final String project2Id = "222";
        final String project2name = "project2";
        final String project2description = "test project2";
        final Status project2status = Status.PLANNED;
        getMap().put(project2Id, new Project(project2Id, project2name, project2description, project2status));
    }

    @NotNull
    public String getProjectIdByName(final String projectName) {
        for (Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return "";
    }

    @Override
    public void persist(final Project project) {
        if (isExist(project)) return;
        getMap().put(project.getId(), project);
    }

    public void update(final Project project) {
        for (Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (project.getId().equals(entry.getKey())) {
                entry.getValue().setName(project.getName());
                entry.getValue().setDescription(project.getDescription());
            }
        }
    }
}
